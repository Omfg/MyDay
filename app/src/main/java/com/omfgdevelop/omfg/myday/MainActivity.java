package com.omfgdevelop.omfg.myday;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
public static final String LOG_TAG = "myLog";
    ArrayList<Task> taskList = new ArrayList<>();
    ArAdapter adapter = null;
    static ArrayList<String> resultRow;
    JSONArray tasksArray = new JSONArray();

    public class ArAdapter extends ArrayAdapter<Task> {
        public ArAdapter() {
            super(MainActivity.this, R.layout.activity_main, taskList);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        new ParseTask().execute();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    private class ParseTask extends AsyncTask<Void, Void, String> {
        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;
        String resultJson = "";


        @Override
        protected String doInBackground(Void... voids) {
            try {
                URL url = new URL("http://plantinlamp.ru/daytask.json");

                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

                InputStream inputStream = urlConnection.getInputStream();
                Log.d(LOG_TAG,urlConnection.getInputStream().toString());
                StringBuffer buffer = new StringBuffer();

                reader = new BufferedReader(new InputStreamReader(inputStream));

                String line;
                while ((line = reader.readLine()) != null) {
                    buffer.append(line);
                }
                resultJson = buffer.toString();
                Log.d(LOG_TAG,resultJson);
            } catch (Exception e) {
                e.printStackTrace();
            }


            return resultJson;


        }

        @Override
        protected void onPostExecute(String strJson) {
            super.onPostExecute(strJson);
            Log.d(LOG_TAG, strJson);
            JSONObject dataJsonObj = null;
            String secondName = "";

            try{
                dataJsonObj = new JSONObject(strJson);
                JSONArray myDay = dataJsonObj.getJSONArray("My_Day");

                for (int i = 0; i < myDay.length(); i++) {
//                    JSONObject task = myDay.getJSONObject(i);
                    JSONObject day = myDay.getJSONObject(i);

                    String task = day.getString("task");
                    String status = day.getString("status");
                    String date = day.getString("date");
                    Log.d(LOG_TAG,task);
                    Log.d(LOG_TAG,status);
                    Log.d(LOG_TAG,date);



                }
            }catch (Exception e){e.printStackTrace();}

        }
    }









    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
